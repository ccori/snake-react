import React, {Component} from 'react';
import PropTypes from 'prop-types';


class Grid extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            size: 15,
            cellStyle: {
                "borderLeft": "1px solid lightgray",
                "borderBottom": "1px solid lightgray"
            },
            snake:[[2, 2],[2,3],[2,4]],
            food:[],
            direction: 40,
        }
        this.drawFood = this.drawFood.bind(this);
        this.moveSnake = this.moveSnake.bind(this);
        this.eatFood = this.eatFood.bind(this);
        this.changeDirection = this.changeDirection.bind(this);
        this.startGame = this.startGame.bind(this);
      }

    drawFood() {
        const x = parseInt(Math.random() * this.state.size);
        const y = parseInt(Math.random() * this.state.size);
        this.setState({ food: [x, y] });
    }

    eatFood() {
        if (this.state.snake[0][0] == this.state.food[0] && this.state.snake[0][1] == this.state.food[1]) {
            let newSnakeElement = [],
                direction;
            for( let i=0; i < this.state.snake.length; i++) {
                if(this.state.snake[i+1] !== undefined) {
                    if (this.state.snake[i][0] === this.state.snake[i+1][0] && this.state.snake[i][1] > this.state.snake[i+1][1] ) {
                        direction = 'v';
                    } else if (this.state.snake[i][1] === this.state.snake[i+1][1] && this.state.snake[i][0] > this.state.snake[i+1][0]) {
                        direction = 'o';
                    }
                }
            }
            if (direction === 'v') {
                newSnakeElement = [this.state.snake[this.state.snake.length-1][0], this.state.snake[this.state.snake.length-1][1]+ 1];
            } else if (direction === 'o') {
                newSnakeElement = [this.state.snake[this.state.snake.length-1][0]+ 1, this.state.snake[this.state.snake.length-1][1]];
            }
            this.setState({food: [],});
            this.setState(prevState => ({
                snake: [...prevState.snake, newSnakeElement]
              }));

            this.drawFood();
        }
    }

    isInsideGrid(position) {
        return (
            position[0] > -1 &&
            position[1] > -1 &&
            position[0] < this.state.size &&
            position[1] < this.state.size
         );
    }

    changeDirection(e) {
        if([38,40].indexOf(this.state.direction) !== -1) {
            switch(e.keyCode) {
                case 40: //down
                case 38: //up
                    break;
                case 39: //right
                case 37: //left
                    this.setState({direction : e.keyCode});
                    break;
            }
        }
        if([37,39].indexOf(this.state.direction) !== -1) {
            switch(e.keyCode) {
                case 39: //right
                case 37: //left
                    break;
                case 40: //down
                case 38: //up
                    this.setState({direction : e.keyCode});
                    break;
            }
        }
    }

    moveSnake() {
        let newSnake = [];
        let prevPos = this.state.snake[0];
        for (let i = 0; i < this.state.snake.length; i++) {
            
            if (i == 0) {
                switch (this.state.direction) {
                    case 40: //down
                        newSnake[i] = [this.state.snake[i][0] + 1, this.state.snake[i][1]];
                        break;
                    case 38: //up
                        newSnake[i] = [this.state.snake[i][0] - 1, this.state.snake[i][1]];
                        break;
                    case 39: //right
                        newSnake[i] = [this.state.snake[i][0], this.state.snake[i][1] + 1];
                        break;
                    case 37: //left
                        newSnake[i] = [this.state.snake[i][0], this.state.snake[i][1] - 1];
                        break;
                }
            } else {
                newSnake[i] = prevPos;
                prevPos = this.state.snake[i];
            }
        }
        
        if(this.isInsideGrid(newSnake[0])) {
            this.setState({snake:newSnake});
        } else { 
            alert ('You lost');
        }
    }

    startGame() {
        setInterval(this.moveSnake, 200);
    }

    componentDidMount() {
        this.drawFood();
    }

    componentWillUpdate() {
        this.eatFood();
    }

    render() {
        console.log(this.state.snake);
        let tableStyle = {
            align: "center",
            width: "640px",
            border: "1px solid",
            height: "640px"
        };

        let rows = [];

        for (var i = 0; i < this.state.size; i++) {
            let rowID = `row${i}`
            let cell = []
            let filledCells = []
            for (var j = 0; j < this.state.size; j++) {
                let cellID = `cell${i}${j}`
                for (var k = 0; k < this.state.snake.length; k++) {
                    if (i == this.state.snake[k][0] && j == this.state.snake[k][1]) {
                        cell.push(<td key={cellID} id={cellID} bgcolor='red' style={this.state.cellStyle}></td>)
                        filledCells.push(cellID);
                    }
                }
                if ( filledCells.indexOf(cellID) === -1 ) {
                    if (i == this.state.food[0] && j == this.state.food[1]) {
                        cell.push(<td key={cellID} id={cellID} bgcolor='blue' style={this.state.cellStyle}></td>)
                    } else {
                        cell.push(<td key={cellID} id={cellID} style={this.state.cellStyle}></td>)
                    }
                }
                
            }
            rows.push(<tr key={i} id={rowID}>{cell}</tr>)
        }

        return (
            <div className="schedule padd-lr" onKeyDown={this.changeDirection} onClick={this.startGame}tabIndex="0">
                <table cellSpacing="1" id="mytable" style={tableStyle}>
                    <tbody>{rows}</tbody>
                </table>
            </div>
        );

    }
}

export default Grid;
