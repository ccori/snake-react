import React from 'react';
import ReactDOM from 'react-dom';
import Grid from './components/Grid'

const MainComponent = () => {
    return (
        <div>
            <h1>Snake Game React</h1>
            <Grid/>
        </div>
    );
}

ReactDOM.render(<MainComponent />, document.getElementById('root'));